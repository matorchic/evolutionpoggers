export interface IPageController {
    showTotal(count: number);
    showPassive(count: number);
    showAggressive(count: number);
    showDead(count: number);
}