import {WorldController} from "../simulation/controller/world/WorldController";
import {IPageController} from "./IPageController";
import {IWorldController} from "../simulation/controller/world/IWorldController";
import {CreatureType} from "../simulation/objects/creatures/enum/CreatureType";
import {Point} from "../simulation/objects/geometry/Point";
import {DrawController} from "../simulation/controller/draw/DrawController";
import {IDrawController} from "../simulation/controller/draw/IDrawController";

export class PageController implements IPageController {
    private worldController: IWorldController;
    private drawController: IDrawController;

    // HTML elements
    canvas: HTMLCanvasElement;
    graphCanvasMap: Map<CreatureType, HTMLCanvasElement>;
    // Buttons
    startButton: HTMLElement;
    pauseButton: HTMLElement;
    refreshButton: HTMLElement;
    // Text fields
    totalCountField: HTMLElement;
    passiveCountField: HTMLElement;
    aggressiveCountField: HTMLElement;
    deadCountField: HTMLElement;

    constructor() {
        // Initializing bttns and stuff
        this.startButton = document.getElementById('bttn_start');
        this.pauseButton = document.getElementById('bttn_pause');
        this.refreshButton = document.getElementById('bttn_refresh');

        this.totalCountField = document.getElementById('total_count');
        this.passiveCountField = document.getElementById('passive_count');
        this.aggressiveCountField = document.getElementById('aggressive_count');
        this.deadCountField = document.getElementById('dead_count');

        this.canvas = document.getElementById('main_draw') as HTMLCanvasElement;
        this.graphCanvasMap = new Map<CreatureType, HTMLCanvasElement>([
            [CreatureType.GRASS, document.getElementById('Grass') as HTMLCanvasElement],
            [CreatureType.HERBIVOROUS, document.getElementById('Herbivorous') as HTMLCanvasElement],
        ]);
        this.drawController = new DrawController(
            this.canvas,
            this.graphCanvasMap,
            this,
        );


        console.log(this.drawController);
        this.worldController = new WorldController(
            [100, 100],
            this.drawController,
            this,
            200,
            0,
            0,
        );
        console.log(this.worldController);
        this.startButton.addEventListener('click', this.startSimulation.bind(this));
        this.pauseButton.addEventListener('click', this.pauseSimulation.bind(this));
        this.refreshButton.addEventListener('click', this.refreshSimulation.bind(this));
    }

    startSimulation() { this.worldController.start(); }
    pauseSimulation() { this.worldController.pause(); }
    refreshSimulation() { this.worldController.refresh(); }

    showAggressive(count: number) {
        this.aggressiveCountField.innerHTML = String(count);
    }

    showDead(count: number) {
        this.deadCountField.innerHTML = String(count);
    }

    showPassive(count: number) {
        this.passiveCountField.innerHTML = String(count);
    }

    showTotal(count: number) {
        this.totalCountField.innerHTML = String(count);
    }
}