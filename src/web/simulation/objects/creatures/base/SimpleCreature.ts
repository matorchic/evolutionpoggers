import {CreatureType} from "../enum/CreatureType";
import {Point} from "../../geometry/Point";
import {ICreature} from "./ICreature";
import {CreatureStatus} from "../enum/CreatureStatus";
import {ICreatureDrawManager} from "../../../controller/draw/creature/ICreatureDrawManager";
import {CreatureDrawManager} from "../../../controller/draw/creature/CreatureDrawManager";
import {IWorldController} from "../../../controller/world/IWorldController";

export class SimpleCreature implements ICreature{
    type: CreatureType;
    position: Point;
    status: CreatureStatus;
    drawManager: ICreatureDrawManager;

    constructor(
        public birthPoint: Point,
        public worldController: IWorldController,
    ) {
        this.type = CreatureType.SIMPLE;
        this.birthPoint = birthPoint;
        this.position = this.birthPoint;
        this.status = CreatureStatus.ALIVE;
        this.drawManager = new CreatureDrawManager(this);
    }
    // TODO use interface for data
    cycle(
        grid: ICreature[][][],
    ) {
    }

    afterCycle(gridCell: ICreature[]) {
    }

    beforeCycle() {
    }
    simulateDefence(): number {
        return 0;
    }
    die() {
        this.worldController.removeCreature(this);
    }
    // This helps to avoid circular links between objects
    onDestroy() {
        this.drawManager = null;
        this.worldController = null;
    }
}