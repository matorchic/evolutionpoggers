import {SimpleCreature} from "./base/SimpleCreature";
import {ICreature} from "./base/ICreature";
import {Point} from "../geometry/Point";
import {IWorldController} from "../../controller/world/IWorldController";
import {CreatureType} from "./enum/CreatureType";
import {GrassCreatureDrawManager} from "../../controller/draw/creature/GrassCreatureDrawManager";

export class GrassCreature extends SimpleCreature implements ICreature {
    public static SPAWN_CHANCE_THRESHOLD = 0.9995;

    public nutrition: number;
    private static NUTRITION_MIN = 10;
    private static NUTRITION_DELTA = 50;


    constructor(
        birthPoint: Point,
        worldController: IWorldController,
    ) {
        super(birthPoint, worldController);
        this.nutrition = GrassCreature.generateNutrition();
        this.type = CreatureType.GRASS;
        this.drawManager = new GrassCreatureDrawManager(this);
    }

    private static generateNutrition() {
        return Math.random()*GrassCreature.NUTRITION_DELTA + GrassCreature.NUTRITION_MIN;
    }

    giveNutrition(requestedAmount: number) {
        const currentNutrition = this.nutrition;
        this.nutrition = Math.max(0, currentNutrition - requestedAmount);
        return Math.min(currentNutrition, requestedAmount);
    }

    getNutritionPercentage(): number {
        return this.nutrition / (GrassCreature.NUTRITION_MIN + GrassCreature.NUTRITION_DELTA);
    }

    cycle() {
    }

    afterCycle(gridCell: ICreature[]) {
        if (this.nutrition === 0) {
            this.die();
        }
    }
}