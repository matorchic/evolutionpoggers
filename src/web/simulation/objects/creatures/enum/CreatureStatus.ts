export enum CreatureStatus {
    ALIVE = 0,
    DEAD = 1,
}