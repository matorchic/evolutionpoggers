import {ICreature} from "./base/ICreature";
import {LivingCreature} from "./LivingCreature";
import {Point} from "../geometry/Point";
import {IWorldController} from "../../controller/world/IWorldController";
import {CreatureType} from "./enum/CreatureType";
import {GrassCreature} from "./GrassCreature";

/**
 * Can defend itself (sometimes)
 * Can give birth to a new Herbivorous creature if has enough energy
 * Can die if doesn't have energy
 * Can eat grass to receive energy
 */
export class HerbivorousCreature extends LivingCreature implements ICreature {
    private nutrition: number;
    private static INITIAL_NUTRITION: number = 100;
    private static BIRTH_NUTRITION_THRESHOLD = 150;
    private static BIRTH_NUTRITION_DELTA = 50;
    private static NUTRITION_MAX = 200;
    private static DIE_NUTRITION_THRESHOLD = 0;
    constructor(
        birthPoint: Point,
        worldController: IWorldController,
    ) {
        super(birthPoint, worldController);
        this.type = CreatureType.HERBIVOROUS;
        this.nutrition = HerbivorousCreature.INITIAL_NUTRITION;
    }
    simulateDefence(): number {
        return Math.floor(Math.random() * 100);
    }
    cycle(grid: ICreature[][][]) {
        super.cycle(grid);
    }
    afterCycle(gridCell: ICreature[]) {
        // First, it tries to eat
        const grass: GrassCreature =
            gridCell.find((creature) => creature.type === CreatureType.GRASS) as GrassCreature;
        if (grass) {
            const receivedNutrition: number = grass.giveNutrition(
                HerbivorousCreature.NUTRITION_MAX - this.nutrition,
            );
            this.nutrition += receivedNutrition;
        }
        // Now it checks current nutrition
        if (this.nutrition <= HerbivorousCreature.DIE_NUTRITION_THRESHOLD) {
            super.die();
        } else if (this.nutrition >= HerbivorousCreature.BIRTH_NUTRITION_THRESHOLD) {
            this.giveBirth();
        }
        // Nutrition dissipates
        this.nutrition--;
    }

    /**
     * New creature is spawned on the same cell
     */
    giveBirth() {
        const child = new HerbivorousCreature(this.position, this.worldController);
        this.worldController.addCreature(child);
        this.nutrition -= HerbivorousCreature.BIRTH_NUTRITION_DELTA;
    }
}