import {SimpleCreature} from "./base/SimpleCreature";
import {ICreature} from "./base/ICreature";
import {Direction} from "../geometry/Direction";
import {Point} from "../geometry/Point";
import {DeadCreature} from "./DeadCreature";
import {CreatureType} from "./enum/CreatureType";

/**
 * Moves
 * Tries to avoid other creatures while moving
 * Does not defend itself
 * Can not eat, can not reproduce itself, can not die of old age
 * Dies if attacked by predator, leaves corpse
 */
export class LivingCreature extends SimpleCreature implements ICreature {
    cycle(
        grid: ICreature[][][],
    ) {
        super.cycle(grid);

        const nearbyLeft = this.worldController.getGridCell(this.position.x - 1, this.position.y, grid);
        const nearbyRight = this.worldController.getGridCell(this.position.x + 1, this.position.y, grid);
        const nearbyUp = this.worldController.getGridCell(this.position.x, this.position.y + 1, grid);
        const nearbyDown = this.worldController.getGridCell(this.position.x, this.position.y - 1, grid);
        const freeDirections: Direction[] = [];
        if (LivingCreature.canStepOnCell(nearbyLeft)) {
            freeDirections.push(Direction.LEFT);
        }
        if (LivingCreature.canStepOnCell(nearbyRight)) {
            freeDirections.push(Direction.RIGHT);
        }
        if (LivingCreature.canStepOnCell(nearbyUp)) {
            freeDirections.push(Direction.UP);
        }
        if (LivingCreature.canStepOnCell(nearbyDown)) {
            freeDirections.push(Direction.DOWN);
        }
        const randomizedDirection = freeDirections[
            Math.floor(Math.random() * freeDirections.length)
            ];
        switch (randomizedDirection) {
            case Direction.DOWN: {
                this.position = new Point(this.position.x, this.position.y - 1);
                break;
            }
            case Direction.UP: {
                this.position = new Point(this.position.x, this.position.y + 1);
                break;
            }
            case Direction.LEFT: {
                this.position = new Point(this.position.x - 1, this.position.y);
                break;
            }
            case Direction.RIGHT: {
                this.position = new Point(this.position.x + 1, this.position.y);
                break;
            }
        }
    }
    die() {
        const creatureCorpse = new DeadCreature(this, this.worldController);
        this.worldController.addCreature(creatureCorpse);
        super.die();
    }
    // Service functions
    static canStepOnCell(gridCell: ICreature[]): boolean {
        if (!gridCell) {
            return false;
        }
        return gridCell.findIndex((cr) => cr.type !== CreatureType.GRASS) < 0;
    }
}