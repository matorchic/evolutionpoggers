import {ICreature} from "./base/ICreature";
import {CreatureType} from "./enum/CreatureType";
import {Point} from "../geometry/Point";
import {CreatureStatus} from "./enum/CreatureStatus";
import {IWorldController} from "../../controller/world/IWorldController";
import {CreatureDrawManager} from "../../controller/draw/creature/CreatureDrawManager";
import {ICreatureDrawManager} from "../../controller/draw/creature/ICreatureDrawManager";
import {DeadCreatureDrawManager} from "../../controller/draw/creature/DeadCreatureDrawManager";

export class DeadCreature implements ICreature {
    birthPoint: Point;
    position: Point;
    type: CreatureType;
    status: CreatureStatus;
    drawManager: ICreatureDrawManager;

    private static DEFAULT_CORPSE_LIFETIME = 100;
    private remainingTimeTicks: number;

    constructor(
        initialCreature: ICreature,
        private worldController: IWorldController,
    ) {
        this.birthPoint = initialCreature.birthPoint;
        this.position = initialCreature.position;
        this.type = initialCreature.type;
        this.status = CreatureStatus.DEAD;
        this.remainingTimeTicks = DeadCreature.DEFAULT_CORPSE_LIFETIME;
        this.drawManager = new DeadCreatureDrawManager(this);
    }

    afterCycle(gridCell: ICreature[]) {
        if (!this.remainingTimeTicks) {
            this.worldController.removeCreature(this);
        }
    }

    beforeCycle() {
        this.remainingTimeTicks--;
    }

    cycle() {

    }

    die() {
    }

    simulateDefence(): number {
        return 0;
    }

    onDestroy(): void {
        this.drawManager = null;
        this.worldController = null;
    }

    getCorpsePercentage(): number { return this.remainingTimeTicks / DeadCreature.DEFAULT_CORPSE_LIFETIME }

}