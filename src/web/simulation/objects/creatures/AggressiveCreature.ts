import {Point} from "../geometry/Point";
import {CreatureType} from "./enum/CreatureType";
import {ICreature} from "./base/ICreature";
import {IWorldController} from "../../controller/world/IWorldController";
import {LivingCreature} from "./LivingCreature";

/**
 * Aggressive creatures can kill any other creature
 * that collides with them
 * It tries to attack other creatures. If attack succeeds,
 * and attack value is higher than defence value, other
 * creature is dead
 */
export class AggressiveCreature extends LivingCreature implements ICreature {

    protected static ATTACK_SUCCESS_THRESHOLD = 30;

    constructor(birthPoint: Point, worldController: IWorldController) {
        super(birthPoint, worldController);
        this.type = CreatureType.AGGRESSIVE;
    }

    afterCycle(gridCell: ICreature[]) {
        if (gridCell.length > 1) {
            // There're more creatures in the same cell
            gridCell.forEach((otherCreature) => {
                if (otherCreature !== this) {
                    const attackVal = this.simulateAttack();
                    const defenceVal = otherCreature.simulateDefence();
                    if (attackVal > Math.max(defenceVal, AggressiveCreature.ATTACK_SUCCESS_THRESHOLD)) {
                        otherCreature.die();
                    }
                }
            })
        }
    }

    beforeCycle() {
    }

    simulateAttack(): number {
        return Math.floor(Math.random() * 100);
    }
}