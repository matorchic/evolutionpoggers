import {ICreature} from "../../objects/creatures/base/ICreature";
import {Point} from "../../objects/geometry/Point";

export interface IWorldController {
    start();
    pause();
    refresh();
    removeCreature(creature: ICreature);
    addCreature(creature: ICreature);
    getGridCell(x: number, y: number, grid?: ICreature[][][]): ICreature[]
    getNearCells(center: Point, size: number): ICreature[][][];
    getStepNum(): number;
}