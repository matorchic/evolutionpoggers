import {SimpleCreature} from "../../objects/creatures/base/SimpleCreature";
import {Point} from "../../objects/geometry/Point";
import {DrawController} from "../draw/DrawController";
import {ICreature} from "../../objects/creatures/base/ICreature";
import {AggressiveCreature} from "../../objects/creatures/AggressiveCreature";
import {IWorldController} from "./IWorldController";
import {IPageController} from "../../../ui/IPageController";
import {IDrawController} from "../draw/IDrawController";
import {HerbivorousCreature} from "../../objects/creatures/HerbivorousCreature";
import {GrassCreature} from "../../objects/creatures/GrassCreature";
import {CreatureType} from "../../objects/creatures/enum/CreatureType";

export class WorldController implements IWorldController {
    worldSize: number[];
    private static DEFAULT_SIMPLE_CREATURES_COUNT: number = 150;
    private static DEFAULT_AGGRESSIVE_CREATURES_COUNT: number = 50;
    private static DEFAULT_GRASS_CREATURES_COUNT: number = 50;
    initialSimpleCreaturesCount: number;
    initialAggressiveCreaturesCount: number;
    creatures: Array<ICreature>;
    creaturesGrid: ICreature[][][];

    simulationStepNum: number = 0;

    private static STEP_TIMEOUT: number = 10;
    private simulationIntervalId;

    private creaturesInitialized: boolean = false;

    constructor(
        worldSize: number[] = [100, 100],
        private drawController: IDrawController,
        private pageController: IPageController,
        initialCreatures: number = WorldController.DEFAULT_SIMPLE_CREATURES_COUNT,
        initialAggressiveCreatures: number = WorldController.DEFAULT_AGGRESSIVE_CREATURES_COUNT,
        initialGrassCreatures: number = WorldController.DEFAULT_GRASS_CREATURES_COUNT,
    ) {
        this.worldSize = worldSize;
        this.initialSimpleCreaturesCount = initialCreatures;
        this.initialAggressiveCreaturesCount = initialAggressiveCreatures;
    }
    start() {
        console.log(this);
        if (!this.creaturesInitialized) {
            this.initWorld();
            this.creaturesInitialized = true;
        }
        this.drawController.display(this.creatures, this.creaturesGrid, this.simulationStepNum);
        this.startSimulation();
    }
    pause() {
        clearInterval(this.simulationIntervalId);
    }
    refresh() {
        this.creatures = [];
        this.creaturesGrid = null;
        this.creaturesInitialized = false;
        clearInterval(this.simulationIntervalId);
        this.simulationIntervalId = undefined;
        this.simulationStepNum = 0;
        this.drawController.display(this.creatures, this.creaturesGrid, this.simulationStepNum);
    }

    initWorld() {
        this.initCreatureGrid();
        this.initCreatures();
        this.spawnGrass();
    }
    // Grid functions
    // TODO make separate grid manager
    initCreatureGrid() {
        this.creaturesGrid = [];
        for (let x = 0; x < this.worldSize[0]; x++) {
            const gridColumn: SimpleCreature[][] = [];
            this.creaturesGrid.push(gridColumn);
            for (let y = 0; y < this.worldSize[1]; y++) {
                const gridCell: SimpleCreature[] = [];
                gridColumn.push(gridCell);
            }
        }
    }
    getGridCell(x: number, y: number, grid = this.creaturesGrid): ICreature[] {
        if (!this.inBorders(x,y)) {
            return null;
        }
        try {
            return grid[x][y];
        } catch (e) {
            console.log(`Got exc while trying to access ${x} ${y}`);
            console.log(grid);
            throw e;
        }
    }
    // TODO refactor
    initCreatures() {
        this.creatures = [];
        // Generating Herbivorous creatures
        for (let i = 0; i < this.initialSimpleCreaturesCount; i++) {
            this.addCreature(new HerbivorousCreature(this.getFreeCell(), this));
        }
        // Generating Aggressive creatures
        for (let i = 0; i < this.initialAggressiveCreaturesCount; i++) {
            this.addCreature(new AggressiveCreature(this.getFreeCell(), this));
        }
        console.log(`Generated ${this.creatures.length} creatures`);
    }
    getFreeCell(): Point {
        let freeCell: Point = this.getRandomPoint();
        if (!this.inBorders(freeCell)) {
            throw `Generated cell not in borders! ${freeCell.x} ${freeCell.y}`
        }
        while (
            this.creatures.find(
                (savedCreature) =>
                    savedCreature.birthPoint.x === freeCell.x &&
                    savedCreature.birthPoint.y === freeCell.y
            )
            ) {
            freeCell = this.getRandomPoint();
        }
        return freeCell;
    }
    startSimulation() {
        this.simulationIntervalId = setInterval(this.timeStep.bind(this), WorldController.STEP_TIMEOUT);
    }
    timeStep() {
        this.creatures.forEach((creature) => creature.beforeCycle());
        const creaturesGridBeforeStep = this.creaturesGrid;
        this.initCreatureGrid();
        for (let i = 0; i < this.creatures.length; i++) {
            // TODO avoid conflicts
            const creature = this.creatures[i];
            creature.cycle(
                creaturesGridBeforeStep,
            );
            this.creaturesGrid[creature.position.x][creature.position.y].push(creature);
        }
        this.creatures.forEach((creature) => {
            creature.afterCycle(
                this.creaturesGrid[creature.position.x][creature.position.y],
            );
        });
        this.spawnGrass();
        this.simulationStepNum++;
        this.drawController.display(this.creatures, this.creaturesGrid, this.simulationStepNum);
    }
    spawnGrass() {
        for (let x = 0; x < this.worldSize[0]; x++) {
            for (let y = 0; y < this.worldSize[1]; y++) {
                // Tries to spawn grass on every cell that doesn't currently have grass
                const cellHasGrass: boolean = this.creaturesGrid[x][y].findIndex(
                    (creature) => creature.type === CreatureType.GRASS,
                    ) >= 0;
                if (
                    !cellHasGrass &&
                    Math.random() > GrassCreature.SPAWN_CHANCE_THRESHOLD
                ) {
                    this.addCreature(new GrassCreature(new Point(x,y), this));
                }
            }
        }
    }
    // Outer access functions
    removeCreature(creature: ICreature) {
        this.creatures = this.creatures.filter((remainingCreature) => creature !== remainingCreature);
        this.creaturesGrid[creature.position.x][creature.position.y] =
            this.creaturesGrid[creature.position.x][creature.position.y].filter((remainingCreature) => creature !== remainingCreature);
        creature.onDestroy();
    }
    addCreature(creature: ICreature) {
        this.creatures.push(creature);
        this.creaturesGrid[creature.position.x][creature.position.y].push(creature);
    }
    getNearCells(center: Point, size: number): ICreature[][][] {
        // TODO not implemented yet
        return null;
    }
    getStepNum(): number {
        return this.simulationStepNum;
    }

    // Service functions
    private inBorders(...args: number[] | Point[]) {
        let x;
        let y;
        if (args[0] instanceof Point) {
            x = args[0].x;
            y = args[0].y;
        } else {
            x = args[0];
            y = args[1];
        }
        return (
            x >= 0 &&
            x < this.worldSize[0] &&
            y >= 0 &&
            y < this.worldSize[1]
        );
    }
    private getRandomPoint(): Point {
        return new Point(
            Math.floor(Math.random() * this.worldSize[0]),
            Math.floor(Math.random() * this.worldSize[1])
        );
    }
}