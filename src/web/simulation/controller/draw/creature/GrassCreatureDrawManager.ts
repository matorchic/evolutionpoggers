import {CreatureDrawManager} from "./CreatureDrawManager";
import {ICreatureDrawManager} from "./ICreatureDrawManager";
import Color from "color";
import {CreatureColourConfig} from "../colour/CreatureColourConfig";
import {DeadCreature} from "../../../objects/creatures/DeadCreature";
import {GrassCreature} from "../../../objects/creatures/GrassCreature";

export class GrassCreatureDrawManager
    extends CreatureDrawManager
    implements ICreatureDrawManager
{
    constructor(
        creature: GrassCreature,
    ) {
        super(creature);
    }
    getColour(): Color {
        return CreatureColourConfig
            .CREATURE_COLOURS
            .get(this.creature.type)
            .alpha((this.creature as GrassCreature).getNutritionPercentage());
    }
}