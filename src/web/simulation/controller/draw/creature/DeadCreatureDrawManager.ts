import {CreatureDrawManager} from "./CreatureDrawManager";
import {ICreatureDrawManager} from "./ICreatureDrawManager";
import Color from "color";
import {CreatureColourConfig} from "../colour/CreatureColourConfig";
import {DeadCreature} from "../../../objects/creatures/DeadCreature";

export class DeadCreatureDrawManager
    extends CreatureDrawManager
    implements ICreatureDrawManager
{
    constructor(
        creature: DeadCreature,
    ) {
        super(creature);
    }
    getColour(): Color {
        return CreatureColourConfig
            .DEAD_CREATURE_COLOUR
            .alpha((this.creature as DeadCreature).getCorpsePercentage());
    }
}