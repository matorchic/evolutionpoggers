import {ICreature} from "../../../objects/creatures/base/ICreature";
import {ICreatureDrawManager} from "./ICreatureDrawManager";
import {CreatureColourConfig} from "../colour/CreatureColourConfig";
import Color from "color";

export class CreatureDrawManager implements ICreatureDrawManager {
    constructor(
        protected creature: ICreature,
    ) {
    }

    getColour(): Color {
        return CreatureColourConfig.CREATURE_COLOURS.get(this.creature.type);
    }
}