import Color from "color";

export interface ICreatureDrawManager {
    getColour(): Color;
}