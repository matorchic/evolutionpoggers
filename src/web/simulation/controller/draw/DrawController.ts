import {CreatureType} from "../../objects/creatures/enum/CreatureType";
import {Point} from "../../objects/geometry/Point";
import {IPageController} from "../../../ui/IPageController";
import {ICreature} from "../../objects/creatures/base/ICreature";
import {IDrawController} from "./IDrawController";
import {CreatureStatus} from "../../objects/creatures/enum/CreatureStatus";
import Color from "color";
import {IGraphController} from "./graph/IGraphController";
import {GraphController} from "./graph/GraphController";
import {CreatureColourConfig} from "./colour/CreatureColourConfig";

/**
 * This controller is responsible for both drawing
 * and displaying info  to the actual page
 */
export class DrawController implements IDrawController {
    // Drawing fields
    private static CREATURE_DRAW_SIZE: number = 10;

    drawContext: CanvasRenderingContext2D;

    // Graph controllers
    graphControllerMap: Map<CreatureType, IGraphController>;

    constructor(
        private mainCanvas: HTMLCanvasElement,
        private graphCanvasMap: Map<CreatureType, HTMLCanvasElement>,
        private pageController: IPageController,
    ) {
        this.drawContext = mainCanvas.getContext('2d');
        this.graphControllerMap = new Map<CreatureType, IGraphController>();
        this.graphControllerMap.set(
            CreatureType.GRASS,
            new GraphController(
                this.graphCanvasMap.get(CreatureType.GRASS),
                {
                    chartName: 'Grass',
                    chartColor: CreatureColourConfig.CREATURE_COLOURS.get(CreatureType.GRASS),
                }
            ),
        );
        this.graphControllerMap.set(
            CreatureType.HERBIVOROUS,
            new GraphController(
                this.graphCanvasMap.get(CreatureType.HERBIVOROUS),
                {
                    chartName: 'Herbivorous',
                    chartColor: CreatureColourConfig.CREATURE_COLOURS.get(CreatureType.HERBIVOROUS),
                }
            ),
        );
    }
    display(
        creatures: ICreature[],
        creaturesGrid: ICreature[][][],
        stepNum: number,
    ) {
        this.draw(creatures, creaturesGrid);
        this.displayInfo(creatures);
        this.drawGraphs(creatures, stepNum);
    }
    draw(creatures: ICreature[], creaturesGrid: ICreature[][][]) {
        this.drawContext.clearRect(0,0, this.mainCanvas.width, this.mainCanvas.height);
        creatures.forEach((creature) => {
            const creaturesGridCell = creaturesGrid[creature.position.x][creature.position.y];
            if (
                creaturesGridCell.length > 1
            ) {
                // Creatures collision occurred, we have to indicate this
                this.drawContext.fillStyle =
                    DrawController
                        .mixColors(
                            creaturesGridCell
                                .map((creature) => creature.drawManager.getColour()),
                        )
                        .toString();
            } else {
                // No collision, just using common creature colour
                this.drawContext.fillStyle = creature.drawManager.getColour().toString();
            }
            const rectCorner = DrawController.mapCoords(
                creature.position,
                DrawController.CREATURE_DRAW_SIZE,
            );
            this.drawContext.fillRect(
                rectCorner.x,
                rectCorner.y,
                DrawController.CREATURE_DRAW_SIZE,
                DrawController.CREATURE_DRAW_SIZE,
            )
        })
    }
    displayInfo(creatures: ICreature[]) {
        const totalCreaturesCount: number = creatures.length;
        let deadCreaturesCount: number = 0;
        let passiveCreaturesCount: number = 0;
        let aggressiveCreaturesCount: number = 0;
        creatures.forEach((creature) => {
            creature.status === CreatureStatus.DEAD ? (deadCreaturesCount++) :
            creature.type === CreatureType.AGGRESSIVE ? (aggressiveCreaturesCount++) :
            (passiveCreaturesCount++)
        });
        this.pageController.showTotal(totalCreaturesCount);
        this.pageController.showDead(deadCreaturesCount);
        this.pageController.showPassive(passiveCreaturesCount);
        this.pageController.showAggressive(aggressiveCreaturesCount);
    }
    drawGraphs(
        creatures: ICreature[],
        stepNum: number,
    ) {
        this.graphControllerMap.forEach((controller, creatureType) => {
            const creatureCount = creatures.reduce(
                (counter: number, creature: ICreature) => creature.type === creatureType ? (counter + 1) : counter,
                0
            );
            controller.addPoint(new Point(
                stepNum,
                creatureCount,
            ))
        })
    }
    // Service functions
    static mapCoords(creatureCoords: Point, creatureSize: number): Point {
        return new Point(
            creatureCoords.x * creatureSize,
            creatureCoords.y * creatureSize,
        )
    }
    static mixColors(colors: Color[]): Color {
        const res = {
                r: 0,
                g: 0,
                b: 0,
            };
        const multiplier: number = 1 / colors.length;
        colors.forEach((color) => {
            res.r += color.red() * multiplier;
            res.g += color.green() * multiplier;
            res.b += color.blue() * multiplier;
        });
        return Color(res);
    }
}