import {IGraphConfig} from "./IGraphConfig";
import {IGraphController} from "./IGraphController";
import {Point} from "../../../objects/geometry/Point";
import { Chart, ChartPoint } from "chart.js";

export class GraphController implements IGraphController {

    private chart: Chart;

    constructor(
        private canvas: HTMLCanvasElement,
        private config: IGraphConfig,
    ) {
        const initialData: ChartPoint[] = [];
        this.chart = new Chart(canvas, {
            type: 'line',
            data: {
                labels: [],
                datasets: [{
                    pointRadius: 0,
                    label: config.chartName,
                    data: initialData,
                }]
            },
        })
    }

    addPoint(point: Point) {
        this.chart.data.labels.push(point.x);
        this.chart.data.datasets[0].data.push(point.y);
        this.chart.update({ duration: 0 });
    }
}