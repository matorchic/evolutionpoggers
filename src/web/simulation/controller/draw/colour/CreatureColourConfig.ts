import {CreatureType} from "../../../objects/creatures/enum/CreatureType";
import Color from "color";

export class CreatureColourConfig {
    public static CREATURE_COLOURS: Map<CreatureType, Color> = new Map<CreatureType, Color>(
        [
            [
                CreatureType.UNKNOWN,
                Color('#000000'),
            ],
            [
                CreatureType.SIMPLE,
                Color('#0000FF'),
            ],
            [
                CreatureType.AGGRESSIVE,
                Color('#FF0000'),
            ],
            [
                CreatureType.HERBIVOROUS,
                Color('#FFFF00'),
            ],
            [
                CreatureType.GRASS,
                Color('#00FF00'),
            ],
        ]
    );
    public static DEAD_CREATURE_COLOUR: Color = new Color('#555555');
    // public static CREATURE_COLLISION_COLOUR: Color = new Color('#00FF00');
}