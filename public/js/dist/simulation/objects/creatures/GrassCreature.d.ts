import { SimpleCreature } from "./base/SimpleCreature";
import { ICreature } from "./base/ICreature";
import { Point } from "../geometry/Point";
import { IWorldController } from "../../controller/world/IWorldController";
export declare class GrassCreature extends SimpleCreature implements ICreature {
    static SPAWN_CHANCE_THRESHOLD: number;
    nutrition: number;
    private static NUTRITION_MIN;
    private static NUTRITION_DELTA;
    constructor(birthPoint: Point, worldController: IWorldController);
    private static generateNutrition;
    giveNutrition(requestedAmount: number): number;
    getNutritionPercentage(): number;
    cycle(): void;
    afterCycle(gridCell: ICreature[]): void;
}
