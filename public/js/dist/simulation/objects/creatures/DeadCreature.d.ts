import { ICreature } from "./base/ICreature";
import { CreatureType } from "./enum/CreatureType";
import { Point } from "../geometry/Point";
import { CreatureStatus } from "./enum/CreatureStatus";
import { IWorldController } from "../../controller/world/IWorldController";
import { ICreatureDrawManager } from "../../controller/draw/creature/ICreatureDrawManager";
export declare class DeadCreature implements ICreature {
    private worldController;
    birthPoint: Point;
    position: Point;
    type: CreatureType;
    status: CreatureStatus;
    drawManager: ICreatureDrawManager;
    private static DEFAULT_CORPSE_LIFETIME;
    private remainingTimeTicks;
    constructor(initialCreature: ICreature, worldController: IWorldController);
    afterCycle(gridCell: ICreature[]): void;
    beforeCycle(): void;
    cycle(): void;
    die(): void;
    simulateDefence(): number;
    onDestroy(): void;
    getCorpsePercentage(): number;
}
