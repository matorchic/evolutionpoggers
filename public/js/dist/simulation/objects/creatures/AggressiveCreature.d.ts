import { Point } from "../geometry/Point";
import { ICreature } from "./base/ICreature";
import { IWorldController } from "../../controller/world/IWorldController";
import { LivingCreature } from "./LivingCreature";
export declare class AggressiveCreature extends LivingCreature implements ICreature {
    protected static ATTACK_SUCCESS_THRESHOLD: number;
    constructor(birthPoint: Point, worldController: IWorldController);
    afterCycle(gridCell: ICreature[]): void;
    beforeCycle(): void;
    simulateAttack(): number;
}
