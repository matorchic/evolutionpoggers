import { CreatureType } from "./CreatureType";
import { Point } from "../geometry/Point";
import { CreatureStatus } from "./CreatureStatus";
import { ICreatureDrawManager } from "../../controller/draw/ICreatureDrawManager";
export interface ICreature {
    type: CreatureType;
    birthPoint: Point;
    position: Point;
    status: CreatureStatus;
    drawManager: ICreatureDrawManager;
    beforeCycle(): void;
    cycle(): void;
    afterCycle(gridCell: ICreature[]): void;
    die(): void;
    simulateDefence(): number;
    onDestroy(): void;
}
