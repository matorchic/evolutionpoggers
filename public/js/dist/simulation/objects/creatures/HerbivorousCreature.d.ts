import { ICreature } from "./base/ICreature";
import { LivingCreature } from "./LivingCreature";
import { Point } from "../geometry/Point";
import { IWorldController } from "../../controller/world/IWorldController";
export declare class HerbivorousCreature extends LivingCreature implements ICreature {
    private nutrition;
    private static INITIAL_NUTRITION;
    private static BIRTH_NUTRITION_THRESHOLD;
    private static BIRTH_NUTRITION_DELTA;
    private static NUTRITION_MAX;
    private static DIE_NUTRITION_THRESHOLD;
    constructor(birthPoint: Point, worldController: IWorldController);
    simulateDefence(): number;
    cycle(grid: ICreature[][][]): void;
    afterCycle(gridCell: ICreature[]): void;
    giveBirth(): void;
}
