import { CreatureType } from "../enum/CreatureType";
import { Point } from "../../geometry/Point";
import { ICreature } from "./ICreature";
import { CreatureStatus } from "../enum/CreatureStatus";
import { ICreatureDrawManager } from "../../../controller/draw/creature/ICreatureDrawManager";
import { IWorldController } from "../../../controller/world/IWorldController";
export declare class SimpleCreature implements ICreature {
    birthPoint: Point;
    worldController: IWorldController;
    type: CreatureType;
    position: Point;
    status: CreatureStatus;
    drawManager: ICreatureDrawManager;
    constructor(birthPoint: Point, worldController: IWorldController);
    cycle(grid: ICreature[][][]): void;
    afterCycle(gridCell: ICreature[]): void;
    beforeCycle(): void;
    simulateDefence(): number;
    die(): void;
    onDestroy(): void;
}
