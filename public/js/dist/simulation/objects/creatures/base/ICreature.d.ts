import { CreatureType } from "../enum/CreatureType";
import { Point } from "../../geometry/Point";
import { CreatureStatus } from "../enum/CreatureStatus";
import { ICreatureDrawManager } from "../../../controller/draw/creature/ICreatureDrawManager";
export interface ICreature {
    type: CreatureType;
    birthPoint: Point;
    position: Point;
    status: CreatureStatus;
    drawManager: ICreatureDrawManager;
    beforeCycle(): void;
    cycle(grid: ICreature[][][]): void;
    afterCycle(gridCell: ICreature[]): void;
    die(): void;
    simulateDefence(): number;
    onDestroy(): void;
}
