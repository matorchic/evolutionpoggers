import { CreatureType } from "./CreatureType";
import { Point } from "../geometry/Point";
import { ICreature } from "./ICreature";
import { CreatureStatus } from "./CreatureStatus";
import { ICreatureDrawManager } from "../../controller/draw/ICreatureDrawManager";
import { IWorldController } from "../../controller/world/IWorldController";
export declare class SimpleCreature implements ICreature {
    birthPoint: Point;
    worldController: IWorldController;
    type: CreatureType;
    position: Point;
    status: CreatureStatus;
    drawManager: ICreatureDrawManager;
    constructor(birthPoint: Point, worldController: IWorldController);
    cycle(): void;
    afterCycle(gridCell: ICreature[]): void;
    beforeCycle(): void;
    simulateDefence(): number;
    die(): void;
    onDestroy(): void;
}
