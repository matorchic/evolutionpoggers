export declare enum CreatureType {
    UNKNOWN = 0,
    SIMPLE = 1,
    AGGRESSIVE = 2,
    HERBIVOROUS = 3,
    GRASS = 4
}
