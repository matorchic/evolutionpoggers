import { SimpleCreature } from "./base/SimpleCreature";
import { ICreature } from "./base/ICreature";
export declare class LivingCreature extends SimpleCreature implements ICreature {
    cycle(grid: ICreature[][][]): void;
    die(): void;
    static canStepOnCell(gridCell: ICreature[]): boolean;
}
