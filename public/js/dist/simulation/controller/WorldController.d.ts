import { SimpleCreature } from "../objects/creatures/SimpleCreature";
import { Point } from "../objects/geometry/Point";
import { DrawController } from "./draw/DrawController";
import { ICreature } from "../objects/creatures/ICreature";
export declare class WorldController {
    worldSize: number[];
    private static DEFAULT_SIMPLE_CREATURES_COUNT;
    private static DEFAULT_AGGRESSIVE_CREATURES_COUNT;
    initialSimpleCreaturesCount: number;
    initialAggressiveCreaturesCount: number;
    creatures: Array<ICreature>;
    creaturesGrid: ICreature[][][];
    drawController: DrawController;
    private static STEP_TIMEOUT;
    private simulationIntervalId;
    constructor(worldSize: number[], canvas: HTMLCanvasElement, initialCreatures?: number, initialAggressiveCreatures?: number);
    initWorld(): void;
    initCreatureGrid(): void;
    getGridCell(x: number, y: number, grid?: ICreature[][][]): SimpleCreature[];
    initCreatures(): void;
    getFreeCell(): Point;
    startSimulation(): void;
    timeStep(): void;
    private getRandomPoint;
}
