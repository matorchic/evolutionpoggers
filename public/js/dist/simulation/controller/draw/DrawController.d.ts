import { CreatureType } from "../../objects/creatures/enum/CreatureType";
import { Point } from "../../objects/geometry/Point";
import { IPageController } from "../../../ui/IPageController";
import { ICreature } from "../../objects/creatures/base/ICreature";
import { IDrawController } from "./IDrawController";
import Color from "color";
import { IGraphController } from "./graph/IGraphController";
export declare class DrawController implements IDrawController {
    private mainCanvas;
    private graphCanvasMap;
    private pageController;
    private static CREATURE_DRAW_SIZE;
    drawContext: CanvasRenderingContext2D;
    graphControllerMap: Map<CreatureType, IGraphController>;
    constructor(mainCanvas: HTMLCanvasElement, graphCanvasMap: Map<CreatureType, HTMLCanvasElement>, pageController: IPageController);
    display(creatures: ICreature[], creaturesGrid: ICreature[][][], stepNum: number): void;
    draw(creatures: ICreature[], creaturesGrid: ICreature[][][]): void;
    displayInfo(creatures: ICreature[]): void;
    drawGraphs(creatures: ICreature[], stepNum: number): void;
    static mapCoords(creatureCoords: Point, creatureSize: number): Point;
    static mixColors(colors: Color[]): Color;
}
