import * as Color from 'color';
export interface IGraphConfig {
    chartName: string;
    chartColor: Color;
}
