import { IGraphConfig } from "./IGraphConfig";
import { IGraphController } from "./IGraphController";
import { Point } from "../../../objects/geometry/Point";
export declare class GraphController implements IGraphController {
    private canvas;
    private config;
    private chart;
    constructor(canvas: HTMLCanvasElement, config: IGraphConfig);
    addPoint(point: Point): void;
}
