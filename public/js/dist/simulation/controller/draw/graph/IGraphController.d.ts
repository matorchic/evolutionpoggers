import { Point } from "../../../objects/geometry/Point";
export interface IGraphController {
    addPoint(point: Point): any;
}
