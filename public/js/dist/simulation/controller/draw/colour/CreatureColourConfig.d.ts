import { CreatureType } from "../../../objects/creatures/enum/CreatureType";
import Color from "color";
export declare class CreatureColourConfig {
    static CREATURE_COLOURS: Map<CreatureType, Color>;
    static DEAD_CREATURE_COLOUR: Color;
}
