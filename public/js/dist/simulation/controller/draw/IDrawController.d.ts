import { ICreature } from "../../objects/creatures/base/ICreature";
export interface IDrawController {
    display(creatures: ICreature[], creaturesGrid: ICreature[][][], stepNum: number): any;
}
