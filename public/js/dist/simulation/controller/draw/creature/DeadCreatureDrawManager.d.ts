import { CreatureDrawManager } from "./CreatureDrawManager";
import { ICreatureDrawManager } from "./ICreatureDrawManager";
import Color from "color";
import { DeadCreature } from "../../../objects/creatures/DeadCreature";
export declare class DeadCreatureDrawManager extends CreatureDrawManager implements ICreatureDrawManager {
    constructor(creature: DeadCreature);
    getColour(): Color;
}
