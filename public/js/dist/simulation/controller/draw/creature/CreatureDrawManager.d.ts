import { ICreature } from "../../../objects/creatures/base/ICreature";
import { ICreatureDrawManager } from "./ICreatureDrawManager";
import Color from "color";
export declare class CreatureDrawManager implements ICreatureDrawManager {
    protected creature: ICreature;
    constructor(creature: ICreature);
    getColour(): Color;
}
