import { CreatureDrawManager } from "./CreatureDrawManager";
import { ICreatureDrawManager } from "./ICreatureDrawManager";
import Color from "color";
import { GrassCreature } from "../../../objects/creatures/GrassCreature";
export declare class GrassCreatureDrawManager extends CreatureDrawManager implements ICreatureDrawManager {
    constructor(creature: GrassCreature);
    getColour(): Color;
}
