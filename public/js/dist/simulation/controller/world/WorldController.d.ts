import { Point } from "../../objects/geometry/Point";
import { ICreature } from "../../objects/creatures/base/ICreature";
import { IWorldController } from "./IWorldController";
import { IPageController } from "../../../ui/IPageController";
import { IDrawController } from "../draw/IDrawController";
export declare class WorldController implements IWorldController {
    private drawController;
    private pageController;
    worldSize: number[];
    private static DEFAULT_SIMPLE_CREATURES_COUNT;
    private static DEFAULT_AGGRESSIVE_CREATURES_COUNT;
    private static DEFAULT_GRASS_CREATURES_COUNT;
    initialSimpleCreaturesCount: number;
    initialAggressiveCreaturesCount: number;
    creatures: Array<ICreature>;
    creaturesGrid: ICreature[][][];
    simulationStepNum: number;
    private static STEP_TIMEOUT;
    private simulationIntervalId;
    private creaturesInitialized;
    constructor(worldSize: number[], drawController: IDrawController, pageController: IPageController, initialCreatures?: number, initialAggressiveCreatures?: number, initialGrassCreatures?: number);
    start(): void;
    pause(): void;
    refresh(): void;
    initWorld(): void;
    initCreatureGrid(): void;
    getGridCell(x: number, y: number, grid?: ICreature[][][]): ICreature[];
    initCreatures(): void;
    getFreeCell(): Point;
    startSimulation(): void;
    timeStep(): void;
    spawnGrass(): void;
    removeCreature(creature: ICreature): void;
    addCreature(creature: ICreature): void;
    getNearCells(center: Point, size: number): ICreature[][][];
    getStepNum(): number;
    private inBorders;
    private getRandomPoint;
}
