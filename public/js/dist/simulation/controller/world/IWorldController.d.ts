import { ICreature } from "../../objects/creatures/base/ICreature";
import { Point } from "../../objects/geometry/Point";
export interface IWorldController {
    start(): any;
    pause(): any;
    refresh(): any;
    removeCreature(creature: ICreature): any;
    addCreature(creature: ICreature): any;
    getGridCell(x: number, y: number, grid?: ICreature[][][]): ICreature[];
    getNearCells(center: Point, size: number): ICreature[][][];
    getStepNum(): number;
}
