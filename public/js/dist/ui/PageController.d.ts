import { IPageController } from "./IPageController";
import { CreatureType } from "../simulation/objects/creatures/enum/CreatureType";
export declare class PageController implements IPageController {
    private worldController;
    private drawController;
    canvas: HTMLCanvasElement;
    graphCanvasMap: Map<CreatureType, HTMLCanvasElement>;
    startButton: HTMLElement;
    pauseButton: HTMLElement;
    refreshButton: HTMLElement;
    totalCountField: HTMLElement;
    passiveCountField: HTMLElement;
    aggressiveCountField: HTMLElement;
    deadCountField: HTMLElement;
    constructor();
    startSimulation(): void;
    pauseSimulation(): void;
    refreshSimulation(): void;
    showAggressive(count: number): void;
    showDead(count: number): void;
    showPassive(count: number): void;
    showTotal(count: number): void;
}
