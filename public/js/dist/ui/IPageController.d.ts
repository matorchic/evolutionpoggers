export interface IPageController {
    showTotal(count: number): any;
    showPassive(count: number): any;
    showAggressive(count: number): any;
    showDead(count: number): any;
}
