const path = require('path');

module.exports = {
    entry: './src/web/index.ts',
    output: {
        path: path.join(__dirname, 'public', 'js', 'dist'),
        filename: 'index.js'
    },
    module: {
        rules: [
            { test: /\.css$/, use: 'css-loader' },
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js', '.json', '.tsx'],
    },
    mode: 'development',
}